class IssuersController < ApplicationController

  def index
    @issuers=Issuer.all
  end

  def show
    @issuer=Issuer.find_by(id: params[:id])
    if @issuer.nil?

      @issuers=Issuer.all
      flash[:alert]="Your book was not found"
      redirect_to action: :index
    end
  end

  def check_issuer_validity
    @current_issuer.capacity-=1
    !!@current_issuer.save
  end


  def book
    @current_issuer=Issuer.find_by(id: params[:selected_entity_id])
    if (!check_issuer_validity)
      flash[:alert]="No tickets found"
      redirect_to action: :index
      return
    end

    @demo_user=User.find_by(id: rand(1..10))
    @demo_user.tickets.create!(issuer_id: @current_issuer.id,issuer_name: @current_issuer.name)
    flash[:success]="Ticket Booked Successfully"
    redirect_to action: :index
  end

end


# render only has the capacity to render a view
# where as redirect_to has the capacity to make a fresh request to the mentioned action


# Migrate it to over grape-->(completed)

# Add validation for capacity-->(completed)

# Add proper validations (list down all validation which can be added)(completed){{name length{user and issuer}} {unrealistic capacity} {valid email(user)}}

# Use scope instead of order_by-->(completed)


# Current Issues(Pipeline):

# Polymorphic relationship -> study and integrate(complete)

# Study rspec for testing and implementation

# study about entities
