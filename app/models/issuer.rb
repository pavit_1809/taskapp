class Issuer < ApplicationRecord
  default_scope-> {order(capacity: :desc)}
  validates :capacity, numericality: {only_integers: true, greater_than: -1,less_than: 101}
end
