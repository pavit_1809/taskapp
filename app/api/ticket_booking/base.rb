module TicketBooking
  class Base < Grape::API
    mount TicketBooking::V1::Issuers
    mount TicketBooking::V1::Tickets
  end
end
