module TicketBooking
  module V1
    class Issuers < Grape::API
      version 'v1', using: :path
      format :json
      prefix :api

      # before do
      #   header['Access-Control-Allow-Origin'] = '*'
      #   header['Access-Control-Request-Method'] = '*'
      # end

      resources :issuers do

        #-> Post request to create a new issuer
        desc 'Create a new issuer'
        params do
          requires :name, type: String
          requires :capacity, type: Integer
        end
        post '/new' do
          !!Issuer.create!({
            name: params[:name],
            capacity: params[:capacity]
          })
        end

        #-> Get request to return a list of all issuers
        desc 'Return list of issuers'   #url-> /api/v1/issuers
        get do
          issuers=Issuer.all
          present issuers
        end

      end #-> end of resources
    end #-> end of calss
  end #-> end of module v1
end #-> end of module TicketBooking
