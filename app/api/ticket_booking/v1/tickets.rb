require_relative "./helperUtil.rb"

module TicketBooking
  module V1
    class Tickets < Grape::API
      version 'v1', using: :path
      format :json
      prefix :api

      # before do
      #   header['Access-Control-Allow-Origin'] = '*'
      #   header['Access-Control-Request-Method'] = '*'
      # end

      resources :tickets do

        #-> Generate a new ticket for a random user from the database
        desc 'Create a new ticket for a random user'
        params do
          requires :issuer_id, type: Integer
        end
        post '/new' do
          @selected_issuer=Issuer.find_by(id: params[:issuer_id])
          if (!Helper.check_issuer_validity(@selected_issuer))
            present({message: "No ticekts found",status:404})
            return
          end

          @demo_user=User.find_by(id: rand(1..5))
          @demo_user.tickets.create!(
            issuer_id: @selected_issuer.id,
            issuer_name: @selected_issuer.name)
        end

      end #-> end of resources
    end #-> end of calss
  end #-> end of module v1
end #-> end of module TicketBooking
