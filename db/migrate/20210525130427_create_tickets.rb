class CreateTickets < ActiveRecord::Migration[6.1]
  def change
    create_table :tickets do |t|
      t.integer :issuer_id
      t.integer :issuer_name
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
