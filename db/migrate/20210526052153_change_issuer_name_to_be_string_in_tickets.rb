class ChangeIssuerNameToBeStringInTickets < ActiveRecord::Migration[6.1]
  def change
    change_column :tickets, :issuer_name, :string
  end
end
